import { Route, Router, RouterModule } from '@angular/router';
import { WelcomeComponent } from './welcome/welcome.component';
import { NgModule } from '@angular/core';

const ROUTES: Route[] = [
    { path: 'welcome', component: WelcomeComponent },
    { path: 'feature1', loadChildren: './feature1/feature1-routing.module#Feature1RoutingModule' }
]

@NgModule({
    imports: [ RouterModule.forRoot(ROUTES)],
    exports: [ RouterModule ]
})
export class AppRoutingModule {

}
