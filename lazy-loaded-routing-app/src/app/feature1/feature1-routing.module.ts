import { Route, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { Feature1Component1Component } from './feature1-component1/feature1-component1.component'

const ROUTES: Route[] = [
    { path: '', component: Feature1Component1Component }
]

@NgModule({
    imports: [
        RouterModule.forChild(ROUTES)
    ],
    exports: [
        RouterModule
    ]
})
export class Feature1RoutingModule {

}